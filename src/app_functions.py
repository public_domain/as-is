import os
import pyqrcode  # Generates QRcodes
import logging
import blockies # Generating Token icon
from bitcash import Key, PrivateKeyTestnet
from server import ergon_fulcrum_server, bch_fulcrum_server

logger = logging.getLogger('__main__.' + __name__)


def generate_qr_code_function(string, directory, file_location):
    """Generate QR code for address and WIF
    """

    if not os.path.exists(directory):
        # if the media directory is not present
        # then create it.
        os.makedirs(directory)

    # Generating QRcode for wallet address
    qr = pyqrcode.create(string, mode='binary')

    # Saving QRcode to png file using pypng
    qr.png(file_location, scale=15)

    # Logging the operation to loglevel debug
    logger.debug(f"generate_qr_code: file {file_location} created.")


def get_wif_address(prefix, testnet=False, _wif=None):
    """Get addresss and WIF provided `prefix` and testnet
    """
    try:
        if prefix == "bitcoincash":
            os.environ["FULCRUM_API_MAINNET"] = bch_fulcrum_server
            wallet = Key(_wif)
            wif = wallet.to_wif()
            address = wallet.address
            logger.debug(f"get_wif_address: Generated BCH wallet")
        elif prefix == "ergon":
            os.environ["FULCRUM_API_MAINNET"] = ergon_fulcrum_server
            wallet = Key(_wif)
            wif = wallet.to_wif()
            address = wallet.address
            logger.debug(f"get_wif_address: Generated BCH wallet")
        elif prefix == "bchtest":
            wallet = PrivateKeyTestnet(_wif)
            wif = wallet.to_wif()
            address = wallet.address
            logger.debug(f"get_wif_address: Generated test BCH wallet")
        elif prefix not in ("bitcoin", "bitcoin_test"):
            if not testnet:
                wallet = Wallet(_wif, testnet=False)
                wif = wallet.key.mainnet.wifc
                address = Address.from_string(
                                        wallet.address.mainnet.pubaddr1c
                                        ).to_cash_address(prefix=prefix)
                logger.debug(f"get_wif_address: Generated mainnet wallet")
            else:
                wallet = Wallet(_wif, testnet=False)
                wif = wallet.key.testnet.wifc
                address = Address.from_string(
                                        wallet.address.testnet.pubaddr1c
                                        ).to_cash_address(prefix=prefix)
                logger.debug(f"get_wif_address: Generated testnet wallet")
        else:
            if not testnet:
                wallet = Wallet(_wif, testnet=False)
                address = wallet.address.mainnet.pubaddrbc1_P2WPKH
                wif = wallet.key.mainnet.wifc
                logger.debug(f"get_wif_address: Generated Bitcoin wallet")
            else:
                wallet = Wallet(_wif, testnet=False)
                wif = wallet.key.testnet.wifc
                address = wallet.address.testnet.pubaddrtb1_P2WPKH
                logger.debug(f"get_wif_address: Generated test Bitcoin wallet")
        return {"wif": wif, "address": address, "wallet": wallet}
    except Exception as e:
        logger.debug(f"get_wif_address: inside exception {e}")
        raise e

def truncate_string(string, _num):
    """Truncate string, replace text in middle with dots.
    `_num` represent a number of characters to keep on both sides
    """
    return string[0:_num]+"..."+string[-_num:]


def create_icon_from_string(string,image_path):
    """Create icon from string using Blockies,
    Write png image to path.
    """
    data = blockies.create(string)
    with open(image_path, 'wb') as png:
        png.write(data)
