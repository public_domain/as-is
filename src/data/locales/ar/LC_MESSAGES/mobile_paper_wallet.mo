��    '      T  5   �      `  M   a  F   �     �     �               $  	   2     <     C     Q     _     n     �     �     �     �     �     �     �     �     �     �     �     �          #     *  	   /     9     B  $   H     m  3   v     �     �     �     �  �  �  w   �  u   �  $   p     �     �     �     �     �     	     	     .	     @	  !   U	     w	  	   �	     �	     �	     �	     �	     �	     �	     �	  &   �	     
  /   ,
  !   \
     ~
     �
     �
     �
  
   �
  C   �
       ;        V     p          �                      	   !       %              #                       &   $      
                                                             '                                  "     Changes saved! 
 Please regenerate the wallet
 to start using the new prefix  Please be careful
 changing settings may
 break the app functionality API URL Address Address Prefix Address empty? App version:  Balance:  Cancel Check balance Choose a Coin Click to copy! Coin not supported. Confirm Generating Copied! Delete Done! Error Export Generate Help Keys Mobile Paper Wallet New wallet generated! No Balance found for token. Regenerate wallet! Return Save Save file Settings Start There is an error connecting to API. Token ID Type "{settings["confirm_delete_text"]}" to confirm WIF WIF key Wrong Password! filled 2 Project-Id-Version: 1.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-10 22:10+0300
Last-Translator: Mathieu Virbel <mat@kivy.org>
Language-Team: Mathieu Virbel <mat@kivy.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=(n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5);
X-Generator: Poedit 2.3
  حفظت التغييرات! 
رجاء أعد توليد المحفظة
لاستخدام البادئة الجديدة  احذر عند تغيير الإعدادات
 قد يؤدي ذلك لتعطيل بعض
 وظائف البرنامج عنوان نقطة البيانات العنوان بادئة العناوين هل العنوان فارغ؟ إصدار البرنامج:  الرصيد:  ألغ تحقق من الرصيد اختر عملة انقر للنسخ! العملة غير مدعومة. تأكيد التوليد نُسخ! احذف تم! خطأ صدّر ولّد المساعدة المفاتيح محفظة الجوال الورقية ولدت محفظة جديدة! لم يعثر على رصيد للمصكوكة. أعد توليد المحفظة! عُد احفظ احفظ الملف الإعدادات بداية هناك خطأ في الاتصال مع مزود البيانات. رمز الايصال اكتب "{settings["confirm_delete_text"]}" للتوكيد المفتاح الخاص مفتاح WIF كلمة سر خطأ! ملئ 