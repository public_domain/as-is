# As Is Wallet (MVP)

BCH and Ergon Wallet with token support, early stage of development (MVP). Built using `PyBitcash`, `Kivy` and `Pyhon`.


## ScreenShots

![](https://i.imgur.com/cAalrJ7.mp4)


## What works:

* Generate a Token aware addresses
* Show Balances in Satoshi
* Show fungible token balance
* Send fungible tokens
* Import WIF
* Export historical addresses and keys
* Support for testnet4

## Hopefully later

* Switching balance between Satoshi, BCH
* Support for fiat
* Change the networking API in the App
* NFT support
* Better theme
* Put something here .. 


Based on Bitcash Cashtoken fork, still an open pull request.

### Generating New Keys
As generated walletsare single use wallet, you can generate another one from the Keys screen. 

#### Password for Generating a Wallet
To create a new wallet, user is asked to enter a password so they don't mistakenly hide the older wallet.
Current password is hard coded as `Delete`

## Supported coins

 * Bitcoin Cash (bitcoincash)
 * Ergon


## Local Run on PC

You need to install the following packages for clipboard, and cam access for QR code reader.

* xclip 
* libzbar0


## License
GPL v3 or later
