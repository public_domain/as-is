#!/usr/bin/env python3

"""
    main.py: Main file for the Needy Mobile App.
    The Needy Mobile App uses Kivy framework.
"""

# Kivy imports
from kivy.core.window import Window
from kivy.core.clipboard import Clipboard
from kivy.app import App
from kivy.lang import Builder
from kivy.lang import Observable
from kivy.logger import Logger, LOG_LEVELS
from kivy.clock import Clock

from kivy.properties import (
                            ObjectProperty,
                            StringProperty,
                            BooleanProperty,
                            NumericProperty,
                            ListProperty
                            )

# Kivy uix imports
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.checkbox import CheckBox
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.popup import Popup
from kivy.uix.progressbar import ProgressBar
from kivy.utils import platform
from kivy.uix.image import Image, AsyncImage
from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.spinner import Spinner

# From c4k_qr_example
from qrreader import QRReader

# ~ from applayout import AppLayout
from android_permissions import AndroidPermissions

# Python imports
from functools import partial
from os.path import dirname, join
import webbrowser
import threading
import requests
import datetime
import gettext  # multilanguage support
import sqlite3  # database to store data
import time
import os

# External libraries imports
# from bitcoinaddress import Wallet
# from ecashaddress import convert
from ecashaddress.convert import Address

from bitcash import exceptions as bitcash_exceptions

# Importing peewee database module
from models import (
                    Settings,
                    Wallets,
                    db_save_wallets,
                    db_save_settings,
                    export_keys
                    )

from app_functions import (
    generate_qr_code_function,
    get_wif_address,
    truncate_string,
    create_icon_from_string
)

__author__ = "uak@gitlab"

__version__ = "0.1.1"

KIVY_LOG_LEVEL = os.environ.get('KIVY_LOG_LEVEL', "debug")
Logger.setLevel(LOG_LEVELS[KIVY_LOG_LEVEL])

home_dir = os.path.expanduser("~")

# From C4K Cam for Kivy
try:
    if platform == 'android':
        from jnius import autoclass
        from android.runnable import run_on_ui_thread
        from android import mActivity
        View = autoclass('android.view.View')
        Logger.debug('Platform is android')
        @run_on_ui_thread
        def hide_landscape_status_bar(instance, width, height):
            Logger.debug('hide_landscape_status_bar: in function')
            # width,height gives false layout events, on pinch/spread
            # so use Window.width and Window.height
            if Window.width > Window.height:
                # Hide status bar
                option = View.SYSTEM_UI_FLAG_FULLSCREEN
            else:
                # Show status bar
                option = View.SYSTEM_UI_FLAG_VISIBLE
            mActivity.getWindow().getDecorView().setSystemUiVisibility(option)
    else:
        # Dispose of that nasty red dot, required for gestures4kivy.
        from kivy.config import Config
        Config.set('input', 'mouse', 'mouse, disable_multitouch')
        Logger.debug('Platform not android')
except Exception as e:
    raise e

# Variables
app_name = 'mobile_paper_wallet'
database_file = "db.sqlite3"
app_icon_file = "data/icon.png"
address_qrcode_file = "media/address.png"
wif_qrcode_file = "media/wif.png"
media_dir = "media/"
layout_file = 'layout.kv'
wallets_export_file = "wallets.json"
default_coin_prefix = "bitcoincash"
help_link = "https://gitlab.com/uak/mobile-paper-wallet"
custom_font = "data/Amiri-Regular.ttf"  # works with Arabic & English letters
WIF_length = 52  # Compressed WIF length
token_send_fee = 800  # token send fee if not specified


# Password to allow deleting keys
settings = {
    "confirm_delete_text": "delete",
    "app_language": "en",
}

# List of supported coins with their testnet state
supported_coins = {
    "bitcoincash": {"testnet": False},
    "ergon": {"testnet": False},
    "bchtest": {"testnet": True},
}


class MyManager(ScreenManager):
    """The screen manager that handles app screens
    """
    pass


class MainBox(BoxLayout):
    """Mainbox under MainApp
    It contains the ScreenManager
    """
    pass


# gettext support based on https://github.com/tito/kivy-gettext-example
class Lang(Observable):
    """
    Support gettext in Kivy based on `tito` example.
    """
    observers = []
    lang = None

    def __init__(self, defaultlang):
        super(Lang, self).__init__()
        self.ugettext = None
        self.lang = defaultlang
        self.switch_lang(self.lang)

    def _(self, text):
        return self.ugettext(text)

    def fbind(self, name, func, args, **kwargs):
        if name == "_":
            self.observers.append((func, args, kwargs))
        else:
            return super(Lang, self).fbind(name, func, *args, **kwargs)

    def funbind(self, name, func, args, **kwargs):
        if name == "_":
            key = (func, args, kwargs)
            if key in self.observers:
                self.observers.remove(key)
        else:
            return super(Lang, self).funbind(name, func, *args, **kwargs)

    def switch_lang(self, lang):
        # get the right locales directory, and instanciate a gettext
        locale_dir = join(dirname(__file__), 'data', 'locales')
        locales = gettext.translation(app_name, locale_dir, languages=[lang])
        self.ugettext = locales.gettext
        self.lang = lang

        # update all the kv rules attached to this text
        for func, largs, kwargs in self.observers:
            func(largs, None, None)


# Set the language of App
tr = Lang(settings["app_language"])


# Long text
setting_change_warning_txt = tr._(
    """
Please be careful!\nChange settings with caution.
"""
)
wallet_regenerate_warning_txt = tr._(
    """
Changes saved!\nPlease regenerate the wallet to start using the new prefix
"""
)


class SaveDialog(FloatLayout):
    """The file save dialog class
    """
    save_to_storage = ObjectProperty(None)
    text_input = ObjectProperty(None)
    cancel = ObjectProperty(None)


# Used with ScreenManager
class QrCodeScreen(Screen):
    pass


class SettingsScreen(Screen):
    """
    Screen to edit settings
    """
    pass


class InfoPopup(RelativeLayout):
    pass


class ConfirmPopup(RelativeLayout):
    pass


class ImportWIFPopup(RelativeLayout):
    pass


class ChooseCoinPopup(RelativeLayout):
    pass


class TokenListScreen(Screen):
    pass


class SendScreen(Screen):
    pass


class CamLayout(FloatLayout):
    '''Holds the qrcode
    '''
    qr_reader = ObjectProperty()


class SelectTokenPopup(RelativeLayout):
    pass


class CamScreen(Screen):
    """
    Screen to edit settings
    """
    # ~ self.layout = AppLayout()
    # ~ def on_pre_leave(self):
    # ~     self.layout.qr_reader.disconnect_camera()
    pass


# Imported from c4k qr example
class CamButtonsLayout(RelativeLayout):
    normal = StringProperty()
    down = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.normal = 'icons/flash-off.png'
        if platform == 'android':
            self.down = 'icons/flash.png'
        else:
            self.down = 'icons/flash-off.png'

    def on_size(self, layout, size):
        if Window.width < Window.height:
            self.pos = (0, 0)
            self.size_hint = (1, 0.2)
            self.ids.torch.pos_hint = {'center_x': .5, 'center_y': .5}
            self.ids.torch.size_hint = (.2, None)
        else:
            self.pos = (Window.width * 0.8, 0)
            self.size_hint = (0.2, 1)
            self.ids.torch.pos_hint = {'center_x': .5, 'center_y': .5}
            self.ids.torch.size_hint = (None, .2)

    def enable_torch(self, state):
        if platform == 'android':
            if state == 'down':
                torch = 'on'
            else:
                torch = 'off'
            self.cam_layout.qr_reader.torch(torch)


class MainApp(App):
    """
    Main App class
    """
    lang = StringProperty(settings["app_language"])
    coin_prefix = StringProperty()
    prefix_changed_recently = BooleanProperty()

    def on_lang(self, instance, lang):
        tr.switch_lang(lang)

    def ar_shape(self, text):
        """
        Convert Arabic text for non compatible display
        """
        reshaped_text = arabic_reshaper.reshape(text)
        return get_display(reshaped_text)

    def save_settings(self):
        """
        Save settings from configration screen to the database.
        """
        self.update_settings = Settings.update(
            balance_api="",
            token_id="",
            coin_prefix=self.root.ids.settings_screen.ids.coin_prefix.text,
            ).where(Settings._id == 1)
        self.update_settings.execute()
        if self.prefix_match() is False:
            self.prefix_changed_recently = True
            error_text = wallet_regenerate_warning_txt
            self.show_info_popup(error_text)
            Logger.debug("save_settings: Data saved. Prefix mismatch.")
        else:
            Logger.debug("save_settings: Data saved. Prefix not altered.")

    def create_wallet(self, prefix, testnet=False, _wif=None):
        """Create wallet using bitcoinaddress lib then convert it to
        SLP address using cashaddress lib
        """
        # self.app_settings = Settings.get(Settings._id == 1)
        # Create wallet address using ecashaddress lib and set custom prefix
        Logger.debug(f"create_wallet: in wallet generation {prefix}")
        try:
            wallet_dict = get_wif_address(prefix, testnet, _wif)
            self.address = wallet_dict["address"]
            self.wif = wallet_dict["wif"]
            self.key = wallet_dict["wallet"]
            db_save_wallets(self.address,  self.wif)
            Logger.info(f"create_wallet: new wallet address {self.address} .")
            Logger.debug(f"create_wallet: new wallet WIF {self.wif} .")
            self.prefix_changed_recently = False
        except Exception as e:
            error_text = "There is an issue with generating wallet"
            self.show_info_popup(error_text)
            Logger.debug(f"create_wallet: Exception {e}")

    def generate_qr_code(self):
        """Generate QR code for address and WIF
        """
        generate_qr_code_function(
            self.address,
            media_dir,
            address_qrcode_file
        )
        generate_qr_code_function(
            self.wif,
            media_dir,
            wif_qrcode_file
        )
        # Logging the operation to loglevel debug
        Logger.debug(
            f"generate_qr_code: Qrcode file {self.address} and WIF created. "
        )

    def prefix_match(self):
        """Check if database record of current address matches settings
        address prefix.
        """
        self.app_settings = Settings.get(Settings._id == 1)
        _coin_prefix = self.app_settings.coin_prefix
        wallet_records = Wallets.select().order_by(Wallets._id.desc()).get()
        _address = wallet_records.address
        record_prefix = _address.split(":")[0]
        Logger.debug(f"prefix_match: Record_prefix {record_prefix}")
        Logger.debug(
            f"prefix_match: app settings {_coin_prefix}"
        )

        if (
            _coin_prefix not in ("bitcoin", "bitcoin_test")
            and record_prefix == _coin_prefix
        ):
            Logger.debug(
                'prefix_match: Not Bitcoin match.'
            )
            return True
        # Internally ergon is still stored as bitcoincash
        # This should change if bitcash get proper support for Ergon
        elif (
          _coin_prefix in "ergon"):
              return True
        # Check if address is not cashaddress and it's bitcoin address with bc
        elif (
            ":" not in _address
            and _address.startswith("bc")
            and (_coin_prefix == "bitcoin")
        ):
            Logger.debug(
                'prefix_match: wallet starts with bc'
            )
            return True
        elif (
            ":" not in _address
            and _address.startswith("tb")
            and (_coin_prefix == "bitcoin_test")
        ):
            Logger.debug(
                'prefix_match: wallet starts with tb'
            )
            return True
        else:
            Logger.debug(
                f"prefix_match: No match: prefix:{_coin_prefix}"
            )
            return False

    def view_address(self):
        """Address QRcode screen
        """
        # Set source of address qrcode image
        try:
            self.root.qrcode_img.source = address_qrcode_file
            Logger.debug(f"view_address: {address_qrcode_file} loaded")
        except Exception as e:
            Logger.info(f"Unable to load Qrcode file")
            raise e
        Logger.info(f"Address shown: {self.address}")
        # Force reload of image to fix issue not reading file
        self.root.qrcode_img.reload()
        # Wait for some time before continuing
        # time.sleep(0.5)
        # Assign labels
        if self.prefix_match():
            # Make sure ergon address starts with `ergon`
            if self.app_settings.coin_prefix == "ergon":
                self.address = Address.from_string(self.address).to_cash_address(prefix="ergon")
            self.root.ids.sm.qrcode_label.text = self.address
            self.root.ids.sm.qrcode_screen_label.text = "Address"
        else:
            self.root.ids.sm.qrcode_label.text = tr._("Regenerate wallet!")
            Logger.info(f"view_address: Prefix mismatch")
        # Switch to screen0
        self.root.ids.sm.current = 'QrCodeScreen'

    def fetch_balances(self):
        vp_height = self.root.ids.token_list_screen.ids.scrollview_form.viewport_size[1]
        Logger.debug(f"vp_height {vp_height}")
        # ~ print(f"{dir(self.root)=}")
        sv_height = self.root.ids.token_list_screen.ids.scrollview_form.height
        Logger.debug(f"sv_height {sv_height}")
        Logger.debug(f"self.key.address: {self.key.address}")
        try:
            self.balance_layout = GridLayout(cols=3, size_hint_y=None, height='38dp')
            text = self.key.get_balance()
            # ~ label5 = Label(text="Balance: "+text+" sat", size_hint=(1, None), height='38dp')
            coin_amount = str(int(text)/100_000_000)
            balance_label = Label(text="Balance: "+coin_amount+" coin", size_hint=(1, None), height='38dp')
            # ~ self.balance_layout.add_widget(label5)
            self.balance_layout.add_widget(balance_label)
            Logger.debug(f"Balance: Label created")
            self.root.ids.token_list_screen.ids.scrollview_box.add_widget(self.balance_layout)
        except Exception as e:
            Logger.debug(f"Balance: Exception: {e}")
        try:
            token_layout = GridLayout(cols=3, row_force_default=True, row_default_height='40dp')
            self.token_dict = self.get_token_dict()
            Logger.debug(f"token_dict: {self.token_dict}")
            for i, v in self.token_dict.items():
                if 'token_amount' in v:
                    Logger.debug(f"i: {i[0]}  ")
                    Logger.debug(f"v: {v}  ")
                    i_too = truncate_string(i, 5)
                    label1 = Label(
                        text=str(i_too),
                        size_hint=(None, None),
                        height='40dp',
                        width='200dp'
                    )
                    label2 = Label(
                        text=str(v["token_amount"]),
                    )
                    image_path = "media/" + i + ".png"
                    Logger.debug(f"icon path: {image_path}")
                    try:
                        if os.path.isfile(image_path):
                            token_icon = Image(
                                source=str(image_path))
                            Logger.debug(f"icon path: {image_path}  ")
                            Logger.debug(f"it is a file : {image_path}  ")
                        else:
                            Logger.debug(f"it is not a file : {image_path}  ")
                            create_icon_from_string(i, image_path)
                            token_icon = Image(
                                source=str(image_path),
                                size_hint=(None, None),
                                height='40dp',
                                width='30dp'
                            )
                            Logger.debug(f"token_icon: {image_path}  ")
                        token_layout.add_widget(label1)
                        token_layout.add_widget(label2)
                        token_layout.add_widget(token_icon)
                    except Exception as e:
                        print(f"fetch_balances: {e=}")
                        error_text = tr._("There is an issue with token listing!")
                        self.show_info_popup(error_text)

                else:
                    Logger.debug(f"No token_amount, possibly NFT?")
                    pass
            self.root.ids.token_list_screen.ids.scrollview_box.add_widget(token_layout)

        except Exception as e:
            error_text = "Unable to get token info"
            self.show_info_popup(error_text)
            Logger.debug(f"fetch_balances: Exception {e}")

        if vp_height > sv_height:  # otherwise there is no scrolling
            # calculate y value of bottom of scrollview in the viewport
            scroll = self.root.ids.token_list_screen.ids.scrollview_form.scroll_y
            bottom = scroll * (vp_height - sv_height)
            Clock.schedule_once(
                partial(
                    self.adjust_scroll_size,
                    bottom+label.height
                    ), -1)

    def view_token_list(self):
        """Token List screen
        """
        Logger.info(f"Token List shown: {self.address}")
        # Wait for some time before continuing
        time.sleep(0.5)
        self.root.ids.token_list_screen.ids.scrollview_box.clear_widgets()
        self.fetch_balances()

        # Switch to screen0
        self.root.ids.sm.current = 'TokenListScreen'

    def get_token_list(self):
        token_list = []
        for i, v in self.get_token_dict().items():
            if 'token_amount' in v:
                token_list.append(truncate_string(i, 5)+"-"+str(v['token_amount']))
        Logger.debug(f"token_list: {token_list}")
        return token_list

    # ~ def send_screen_func(self):
        # ~ print(f"{self.root.ids.send_screen.ids=}")
        # ~ self.root.ids.send_screen.ids.token_spinner.values = self.get_token_list()

    def view_send(self):
        """Send screen
        """
        # Wait for some time before continuing
        # ~ time.sleep(0.5)
        self.root.ids.token_list_screen.ids.scrollview_box.clear_widgets()
        # ~ self.send_screen_func()

        # Switch to screen0
        self.root.ids.sm.current = 'SendScreen'

    def show_send_again(self):
        self.root.ids.sm.current = 'SendScreen'

    def set_address_from_qrcode(self, text):
        self.root.ids.send_screen.ids.sendto_address.text = text

    def get_token_dict(self):
        self.key.get_balance()
        Logger.debug(f"get_token_dict: wallet address {self.key.address}")
        Logger.debug(f"get_token_dict")
        return self.key.get_cashtokenbalance()

    def show_select_token(self):
        """
        Selection for tokens
        """
        try:
            # Create a new instance of the ConfirmPopup class
            self.token_dict = self.get_token_dict()
            show = SelectTokenPopup()
            box = GridLayout(cols=1, row_force_default=True, row_default_height='40dp')
            # ~ error_label = Label(text=error_text)
            for i, v in self.get_token_dict().items():
                self.token_dict[i]["image_path"] = "media/" + i + ".png"
                layout = RelativeLayout(size_hint_y=None, height='38dp')
                if 'token_amount' in v:
                    text = truncate_string(i, 5)+" #:"+str(v['token_amount'])
                    toggle_button = ToggleButton(
                        text=text,
                        size_hint_y=None,
                        height='38dp',
                        group="coins"
                        )
                    _token = self.token_dict[i]
                    _token["logo_image"] = Image(
                        source=str(_token["image_path"]),
                        size_hint_y=None,
                        width='38dp',
                        height='38dp'
                        )
                    # Using `partial` magic
                    toggle_button.add_widget(self.token_dict[i]["logo_image"])
                    toggle_button.bind(
                        on_release=partial(
                            self.select_token_fn,
                            toggle_button,
                            i
                        )
                    )
                    layout.add_widget(toggle_button)
                    box.add_widget(layout)

            show.add_widget(box)
            # Setup the popup window
            self.popupWindow = Popup(
                                title=tr._("Select Token"),
                                content=show,
                                size_hint=(.9, .6),
                                auto_dismiss=True
                               )

            # Create the popup window
            self.popupWindow.open()  # show the popup
        except Exception as e:
            error_text = "Unable to get token list"
            self.show_info_popup(error_text)
            Logger.debug(f"show_select_token: Exception {e}")

    def send_fn(self):
        """Function to send satoshis and tokens
        """
        try:
            # Convert address to `bitcoincash` as Fulcrum doesn't support other prefixs
            orginal_address = self.root.ids.send_screen.ids.sendto_address.text
            address = Address.from_string(orginal_address).to_cash_address(prefix="bitcoincash")
            send_satoshi_amount = self.root.ids.send_screen.ids.send_bch_amount.text
            Logger.debug(
                f"sending to address: {address} amount: {send_satoshi_amount}"
            )

            token_id = self.root.ids.send_screen.ids.token_spinner.text
            send_token_amount = self.root.ids.send_screen.ids.send_token_amount.text
            Logger.debug(f"Token to send: {token_id}, Amount: {send_token_amount}")
        except Exception as e:
            error_text = tr._(f"exception: {e}")
            self.show_info_popup(error_text)
            raise e

        try:
            # Knowing balance is importatnt to be able to send in pybitcash
            try:
                current_balance = self.key.get_balance('bch')
                Logger.debug(f"Balance obtained: {current_balance}") 
            except Exception as e:
                raise e
            if send_token_amount != "":
                Logger.debug(f"condition met!") 
                # Set fee to 800 sat if not set by user
                if send_satoshi_amount == "":
                    send_satoshi_amount = token_send_fee
                tx_id = self.key.send([(
                            address,
                            int(send_satoshi_amount),
                            "satoshi",
                            token_id,
                            None,
                            None,
                            int(send_token_amount)
                            )])
                Logger.debug(f"txid {tx_id}") 
                self.show_info_popup(tx_id)
            else:
                Logger.debug("token id is not set")
                if send_satoshi_amount != "":
                    tx_id = self.key.send([(
                                address,
                                int(send_satoshi_amount),
                                "satoshi"
                        )])
                    self.show_info_popup(tx_id)
                    Logger.debug(f"txid {tx_id}") 
                else:
                    error_msg = tr._("Amount not specified")
                    self.show_info_popup(error_msg)
        except bitcash_exceptions.InvalidAddress:
            error_text = tr._("Invalid address")
            self.show_info_popup(error_text)
        except Exception as e:
            error_text = tr._(f"send_fn: Exception {e}")
            self.show_info_popup(error_text)
            Logger.debug(error_text)

    def select_token_fn(self, toggle_button, choice, btn):
        self.root.ids.send_screen.ids.token_spinner.text = str(choice)
        self.popupWindow.dismiss()

    def cam_on_start(self):
        self.dont_gc = AndroidPermissions(self.start_cam)

    def show_cam_popup(self):
        """
        Shows cam popup
        """
        try:
            Logger.debug(f"name of layout {self.cam_layout.__class__}")
            Logger.debug(f"doc of layout {self.cam_layout.__doc__}")
            # ~ self.show_camera()
            self.root.ids.cam_screen.ids.cam_box.add_widget(self.cam_layout)
            self.root.ids.sm.current = 'CamScreen'
        except Exception as e:
            error_text = tr._(f"exception: {e}")
            self.show_info_popup(error_text)

    def connect_camera(self, dt):
        Logger.debug(f"connect_camera: inside")
        # ~ self.layout = AppLayout()
        # ~ Logger.debug(f"name of layout {dir(self.layout)}")
        Logger.debug(f"name of layout {self.cam_layout.__class__}")
        Logger.debug(f"doc of layout {self.cam_layout.__doc__}")
        try:
            self.cam_layout.qr_reader.connect_camera(
                enable_analyze_pixels=True,
                enable_video=False
            )
            Logger.debug(f"Camera should have been connected by now")
        except Exception as e:
            Logger.debug(f"Issue while trying to connect Cam. Exception: {e}")
            raise e

    def disconnect_camera(self, dt):
        Logger.debug(f"connect_camera: inside")
        Logger.debug(f"name of layout {self.cam_layout.__class__}")
        Logger.debug(f"doc of layout {self.cam_layout.__doc__}")
        try:
            self.cam_layout.qr_reader.disconnect_camera()
            Logger.debug(f"Camera should have been connected by now")
        except Exception as e:
            Logger.debug(f"Issue while trying to disconnect Cam")
            exit(f"unable to disconnect: Exception: {e}")

    def start_cam(self):
        self.dont_gc = None
        self.cam_layout = CamLayout()
        self.root.ids.cam_screen.ids.cam_box.add_widget(self.cam_layout)
        self.root.ids.sm.current = 'CamScreen'
        # Can't connect camera till after on_start()
        Clock.schedule_once(self.connect_camera)

    def link_popup(self, error_text, explorer):
        """
        Link popup to show clickable links
        """
        # Create a new instance of the ConfirmPopup class
        show = InfoPopup()

        # Setup the popup window
        show.ids.popup_label.text = error_text
        # ~ show.ids.popup_label.on_release = webbrowser.open("http://google.com/")
        popupWindow = Popup(title=tr._("Info"),
                            content=show,
                            size_hint=(.9, .6),
                            auto_dismiss=True
                            )

        # Create the popup window
        popupWindow.open()  # show the popup

    def copy_label(self, dt):
        Clipboard.copy(self.root.ids.sm.qrcode_label.text)
        self.root.ids.qr_code_screen.ids.copy_message.text = tr._("Copied!")
        Logger.debug("wif copied to clipboard")

    def view_wif(self):
        """WIF QRcode screen
        """
        # Set source of WIF QRcode image
        self.root.qrcode_img.source = wif_qrcode_file
        # Force reload of image to fix issue not reading file
        self.root.qrcode_img.reload()
        # Wait for some time before continuing
        time.sleep(0.5)
        # Assign labels
        Logger.debug(f"Wif shown {self.address}")
        self.root.ids.sm.qrcode_screen_label.text = "WIF"
        self.root.ids.sm.qrcode_label.text = self.wif
        # Switch to screen
        self.root.ids.sm.current = 'QrCodeScreen'

    def show_settings(self):
        """Move to settings screen
        """
        error_text = setting_change_warning_txt
        self.show_info_popup(error_text)
        self.app_settings = Settings.get(Settings._id == 1)
        wallet_records = Wallets.select().order_by(Wallets._id.desc()).get()
        self.root.ids.sm.current_record.text = (
            str(wallet_records._id) +
            ", " +
            wallet_records.address
        )
        setting_screen_ids = self.root.ids.settings_screen.ids
        setting_screen_ids.coin_prefix.text = self.app_settings.coin_prefix
        self.root.ids.sm.current = 'SettingsScreen'

    def first_start(self):
        """
        Checks to be done at first start of the App
        """
        try:
            # Create setting database if not exist
            if Settings.select().exists():
                self.app_settings = Settings.get(Settings._id == 1)
                Logger.debug(f"first_start: Got existing settings.")
            else:
                db_save_settings(
                                balance_api="",
                                token_id="",
                                coin_prefix=default_coin_prefix,
                                )
                self.app_settings = Settings.get(Settings._id == 1)
                Logger.debug(f"first_start: Created settings from defaults.")

            # If no entry in database generate wallet and create QRcode
            if Wallets.select().exists() is False:
                self.app_settings = Settings.get(Settings._id == 1)
                self.coin_prefix = self.app_settings.coin_prefix
                self.create_wallet(
                    self.coin_prefix,
                    supported_coins[self.coin_prefix]["testnet"]
                )
                self.generate_qr_code()
                Logger.info(
                    f"no entry was found so new wallet and QR images were created."
                    )
            else:
                # set the wallet address and wif
                wallet = Wallets.select().order_by(Wallets._id.desc()).get()
                self.coin_prefix = self.app_settings.coin_prefix
                self.address = wallet.address
                self.wif = wallet.wif
                wallet_dict = get_wif_address(
                    self.coin_prefix,
                    supported_coins[self.coin_prefix]["testnet"],
                    self.wif
                    )
                self.key = wallet_dict["wallet"]
                # Log it
                Logger.debug(f"first_start: Values used from an existing wallet.")
                Logger.info(f"first_start: Address {self.address}.")
                Logger.debug(f"first_start: WIF {self.wif}.")
            self.view_address()
        except Exception as e:
            raise e

    def show_confirm_popup(self):
        """
        Shows confirmation popup before generating a new wallet and
        replacing the QRcodes with new ones.
        """
        # Create a new instance of the ConfirmPopup class
        self.confirm_popup = ConfirmPopup()

        # Setup the popup window
        popupWindow = Popup(title=tr._("Confirm Generating"),
                            content=self.confirm_popup,
                            size_hint=(.9, .6),
                            auto_dismiss=True
                            )

        # Create the popup window
        popupWindow.open()  # show the popup

    def wif_import_button(self, wif):
        self.coin_prefix = self.root.ids.settings_screen.ids.coin_prefix.text
        self.wif_import_function(
            self.coin_prefix,
            supported_coins[self.coin_prefix]["testnet"],
            wif
        )

    def wif_import_function(self, prefix, testnet, _wif):
        """
        Function to generate wallet from WIF
        """
        if len(_wif) == WIF_length:
            self.create_wallet(prefix, testnet, _wif)
            self.generate_qr_code()
            self.wif_import_popup.popup_text.text = tr._("Done!")
            self.wif_import_popup.import_button.disabled = True
            self.wif_import_popup.popup_text.text = tr._("New wallet generated!")
            self.root.ids.sm.current_record.text = tr._("Recently Changed!")
            Logger.debug("New wallet generated!")
        else:
            self.wif_import_popup.popup_text.text = tr._(
                "Invalid WIF!\nDid you use an uncompressed WIF?"
            )
            self.wif_import_popup.popup_text.halign = 'center'
            Logger.debug("Invalid WIF!")

    def show_import_popup(self):
        """
        Shows confirmation popup before generating a new wallet and
        replacing the QRcodes with new ones.
        """
        # Create a new instance of the ConfirmPopup class
        self.wif_import_popup = ImportWIFPopup()

        # Setup the popup window
        popupWindow = Popup(title=tr._("Confirm Generating"),
                            content=self.wif_import_popup,
                            size_hint=(.9, .6),
                            auto_dismiss=True
                            )

        # Create the popup window
        popupWindow.open()  # show the popup

    def show_info_popup(self, error_text):
        """
        Shows info popup
        """
        # Create a new instance of the ConfirmPopup class
        show = InfoPopup()

        # Setup the popup window
        show.ids.popup_label.text = error_text
        popupWindow = Popup(title=tr._("Info"),
                            content=show,
                            size_hint=(.9, .6),
                            auto_dismiss=True
                            )

        # Create the popup window
        popupWindow.open()  # show the popup

    def show_choose_coin_popup(self):
        """
        Shows choose coin popup
        """
        # Create a new instance of the ConfirmPopup class
        show = ChooseCoinPopup()
        box = StackLayout(orientation="tb-lr")
        for i in supported_coins:
            toggle_button = ToggleButton(text=str(i), group="coins")
            # Using `partial` magic
            toggle_button.bind(
                on_release=partial(self.select_coin, toggle_button, i)
            )
            box.add_widget(toggle_button)
        show.add_widget(box)
        # Setup the popup window
        self.popupWindow = Popup(
                            title=tr._("Choose a Coin"),
                            content=show,
                            size_hint=(.9, .5),
                            auto_dismiss=True
                            )

        # Create the popup window
        self.popupWindow.open()  # show the popup

    def select_coin(self, toggle_button, choice, btn):
        self.root.ids.settings_screen.ids.coin_prefix.text = str(choice)
        self.save_settings()
        self.popupWindow.dismiss()

    def dismiss_popup(self):
        self._popup.dismiss()

    # section for file chooser
    def show_export_window(self):
        """
        Show the export to file dialog
        """
        # Request permission
        try:
            if platform == 'android':
                from android.permissions import request_permissions, Permission
                request_permissions([
                    Permission.READ_EXTERNAL_STORAGE,
                    Permission.WRITE_EXTERNAL_STORAGE
                    ])
                Logger.info('show_export_window: Platform Android.')
            else:
                Logger.info('show_export_window: Platform not Android.')
            content = SaveDialog(
                save_to_storage=self.save_to_storage, cancel=self.dismiss_popup
            )
            self._popup = Popup(title=tr._("Save file"), content=content,
                                size_hint=(0.9, 0.9))
            self._popup.open()
        except Exception as e:
            error_text = "Unable to export keys."
            self.show_info_popup(error_text)
            Logger.info(error_text)

    def on_save_button(self, path, file_name):
        data = export_keys()
        self.save_to_storage(path, file_name, data)

    def save_to_storage(self, path, filename, data):
        """
        Write json formatted wallet keys to storage
        """
        try:
            with open(os.path.join(path, filename), 'w+') as stream:
                stream.write(data)

            self.dismiss_popup()
        except Exception as e:
            error_text = str(e)
            self.show_info_popup(error_text)
            Logger.info(error_text)

    # end section for file chooser ######

    def validate_password(self, password):
        """
        Generate a new wallet and QRcode images if password is correct
        """
        if password == settings["confirm_delete_text"]:
            self.app_settings = Settings.get(Settings._id == 1)
            self.create_wallet(
                self.app_settings.coin_prefix,
                supported_coins[self.app_settings.coin_prefix]["testnet"]
            )
            self.generate_qr_code()
            self.confirm_popup.popup_text.text = tr._("Done!")
            self.confirm_popup.delete_button.disabled = True
            self.confirm_popup.popup_text.text = tr._("New wallet generated!")
            self.root.ids.sm.current_record.text = tr._("Recently Changed")
            Logger.debug("New wallet generated!")
        else:
            self.confirm_popup.popup_text.text = tr._("Wrong password!")
            Logger.debug("Wrong password provided!")

    def build(self):
        self.icon = app_icon_file
        Builder.load_file(layout_file)
        if platform != 'android':
            Window.size = (360, 600)
        return MainBox()

    def on_start(self, **kwargs):
        self.first_start()


if __name__ == '__main__':
    MainApp().run()
